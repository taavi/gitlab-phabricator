#!/usr/bin/env python3

# -*- coding: utf-8 -*-
#
# This file is part of RelEng's gitlab-phabricator application
# Copyright (C) 2022 Ahmon Dancy, Brennen Bearnes, and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
from flask import Flask, request
import json
from sinks.phabricator import PhabricatorSink
import pprint
import yaml

app = Flask(__name__)

config = {
    "debug": False,
    "log_events_to": None,
    "source_token": None,
    "sinks": [],
}

stats = {
    "events_received": 0,
    "events_rejected": 0,
}

# TODO:
# * Logging and stats
# * Robust error handling, handling of missing attributes (some work done, could use a bit more)
# * Deliver asynchronously (decouple event collection from event handling)
# * Add "patch for review" project when a ticket has at least one open task for it.


@app.route('/')
def home():
    return "Please post a JSON message to /hooks"


@app.route('/hooks', methods=["POST"])
def hooks():
    event = request.json
    stats["events_received"] += 1

    if config['debug']:
        pprint.pprint(event)

    if config['log_events_to']:
        with open(config['log_events_to'], "a") as f:
            json.dump(event, f)
            f.write("\n")

    if config["source_token"] and request.headers.get('X-Gitlab-Token') != config["source_token"]:
        stats["events_rejected"] += 1
        return "Invalid token", 400

    for sink in config["sinks"]:
        try:
            sink.notify(event)
        except Exception as e:
            print(f"Notification to {sink} failed: {e}.  Moving on")

    return ''


def load_config(filename):
    global config

    with open(filename) as f:
        c = yaml.safe_load(f)

    config["debug"] = c.get("debug")
    if config["debug"]:
        print("Enabling debug mode (from config)")

    config["log_events_to"] = c.get("log-events-to")
    if config["log_events_to"]:
        print(f"Will log event records to {config['log_events_to']}")

    source = c.get("source")
    if source:
        config["source_token"] = source.get("token")

    sinks = c.get("sinks")
    if not sinks:
        print("No sinks defined.  Nothing will happen for incoming events")
    else:
        for sink in sinks:
            type = sink.get("type")
            if type == "phabricator":
                config["sinks"].append(PhabricatorSink(sink))
            else:
                raise RuntimeError(f"Unsupported sink type: {type}")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config',
                        default='config.yaml')
    args = parser.parse_args()

    print(f"Loading config from {args.config}")
    load_config(args.config)


main()

if __name__ == '__main__':
    app.run(debug=True)
