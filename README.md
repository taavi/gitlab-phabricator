# Gitlab Phabricator

A webhook handler for posting Phabricator comments on GitLab pushes and merge
requests.

See https://wikitech.wikimedia.org/wiki/GitLab/Phabricator_integration for
further details.

## COPYING

Copyright (C) 2023 Ahmon Dancy, Brennen Bearnes, and contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
